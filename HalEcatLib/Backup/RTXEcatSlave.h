#ifndef _RTXECAT_SLAVE_H_
#define _RTXECAT_SLAVE_H_
#include <string>
#include <list>
using namespace std;

#define MAX_RTXPDO_INDEX 32

class CRTXEcatPDO
{
public:
	CRTXEcatPDO()
	{
		m_csName.empty();
		m_csComment.empty();
		m_csTagname.empty();
		m_iDataSize = 0;
		m_iOffset   = 0;
		m_dwGDEOffset = 0;
		m_dwGDEType = 0;
		m_dwByteOffset = 0;
		m_dwRunType = 0;
	};
	~CRTXEcatPDO()
	{
	};


public:
	string m_csName;
	string m_csComment;
	int		m_iOffset;
	int     m_iDataSize;
	//WINPC32 
	string	m_csTagname;
	DWORD	m_dwGDEOffset;
	DWORD	m_dwGDEType;
	DWORD	m_dwByteOffset;
	DWORD	m_dwRunType;

};

typedef list<CRTXEcatPDO *> PDOLIST;

class CRTXEcatSlave
{
public:
	CRTXEcatSlave()
	{
		m_csName.empty();
		m_dwSlaveID				= 0;
		m_dwRevisionID			= 0;
		m_dwEcatType			= 0;
		m_dwPhysicalAddress		= 0;
		m_iInputPDOStartAddress = 0;
		m_iOutpuPDOtStartAddress= 0;
		m_iTxPDONum             = 0;
		m_iRxPDONum             = 0;
		m_dwMotionMode			= 0;
		m_TxPDO.clear();
		m_RxPDO.clear();
		for (int i = 0; i < MAX_RTXPDO_INDEX; i++)
		{
			m_iInputPDOIndex[i]  = 0;
			m_iOutputPDOIndex[i] = 0;
		}
	};

	~CRTXEcatSlave()
	{
		// Clear TX PDO
		PDOLIST::iterator i;
		for (i = m_TxPDO.begin(); i != m_TxPDO.end(); ++i)
		{
			delete (*i);
		}
		m_TxPDO.clear();

		// Clear RX PDO
		for (i = m_RxPDO.begin(); i != m_RxPDO.end(); ++i)
		{
			delete (*i);
		}
		m_RxPDO.clear();

	};

public:
	string  m_csName;
	DWORD   m_dwSlaveID;
	DWORD   m_dwRevisionID;
	DWORD   m_dwEcatType;
	DWORD	m_dwPhysicalAddress;
	DWORD	m_dwMotionMode;

	int     m_iInputPDOStartAddress;
	int     m_iOutpuPDOtStartAddress;

	int		m_iInputPDOIndex[MAX_RTXPDO_INDEX];
	int		m_iOutputPDOIndex[MAX_RTXPDO_INDEX];
	
	int		m_iTxPDONum;
	int		m_iRxPDONum;

	PDOLIST m_TxPDO;
	PDOLIST m_RxPDO;
};

typedef list<CRTXEcatSlave *> SLAVELIST;
class CRTXEcatMaster
{
public: 
	CRTXEcatMaster()
	{
		m_ECatSlave.clear();
	};

	~CRTXEcatMaster()
	{
		m_ECatSlave.clear();
	};

public:
	SLAVELIST m_ECatSlave;
	void	  ClearObject();
	SLAVELIST*  GetList(){ return &m_ECatSlave;}
	void	  PrintOut();
};
#endif //