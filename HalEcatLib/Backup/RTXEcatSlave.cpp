#include "stdafx.h"
#include "RTXEcatSlave.h"
#include <iostream>

void CRTXEcatMaster::ClearObject()
{
	string caName;

	SLAVELIST::iterator pSlavelist;
	for (pSlavelist = m_ECatSlave.begin(); pSlavelist != m_ECatSlave.end(); ++pSlavelist)
	{

		caName = (*pSlavelist)->m_csName;
		/*
		PDOLIST::iterator pTxPDO;
		for (pTxPDO = ((*pSlavelist)->m_TxPDO).begin(); pTxPDO != ((*pSlavelist)->m_TxPDO).end(); ++pTxPDO)
		{
			caName = (*pTxPDO)->m_csName;
			delete (*pTxPDO);
		}

		PDOLIST::iterator pRxPDO;
		for (pRxPDO = (*pSlavelist)->m_RxPDO.begin(); pRxPDO !=(*pSlavelist)->m_RxPDO.end(); ++pRxPDO)
		{
			caName = (*pRxPDO)->m_csName;
			delete (*pRxPDO);
		}
		*/
		delete (*pSlavelist);
	}
}

void CRTXEcatMaster::PrintOut()
{
	FILE *pfile = fopen("\\HDD\\PrintOutRTXFormat.txt", "w");
	string caName;
	SLAVELIST::iterator pSlavelist;
	for (pSlavelist = m_ECatSlave.begin(); pSlavelist != m_ECatSlave.end(); ++pSlavelist)
	{

		caName = (*pSlavelist)->m_csName;
		fprintf(pfile, "[%s]\n",				caName.c_str());
		fprintf(pfile, "Slave ID = 0x%X\n",		(*pSlavelist)->m_dwSlaveID);
		fprintf(pfile, "RevisionID = 0x%X\n",	(*pSlavelist)->m_dwRevisionID);
		fprintf(pfile, "ECAT Type = 0x%X\n",	(*pSlavelist)->m_dwEcatType);
		fprintf(pfile, "Physical Address = %d\n",	(*pSlavelist)->m_dwPhysicalAddress);

		PDOLIST::iterator pTxPDO;
		fprintf(pfile, "=======[TX PDO] ============\n");
		for (pTxPDO = ((*pSlavelist)->m_TxPDO).begin(); pTxPDO != ((*pSlavelist)->m_TxPDO).end(); ++pTxPDO)
		{
			caName = (*pTxPDO)->m_csName;
			fprintf(pfile, "\t%s\n", caName.c_str());
			fprintf(pfile, "\tPDO Offset: %d\n", (*pTxPDO)->m_iOffset);
			fprintf(pfile, "\tGDE Offset: %d\n", (*pTxPDO)->m_dwGDEOffset);
			fprintf(pfile, "\tByte Offset : %d\n", (*pTxPDO)->m_dwByteOffset);
			fprintf(pfile, "\tPDO Type  : %d\n", (*pTxPDO)->m_dwGDEType);
			fprintf(pfile, "\tData Size : %d\n", (*pTxPDO)->m_iDataSize);
			fprintf(pfile, "\tRun Type : %d\n", (*pTxPDO)->m_dwRunType);
			fprintf(pfile, "\n");
		}

		PDOLIST::iterator pRxPDO;
		fprintf(pfile, "=======[RX PDO] ============\n");
		for (pRxPDO = (*pSlavelist)->m_RxPDO.begin(); pRxPDO !=(*pSlavelist)->m_RxPDO.end(); ++pRxPDO)
		{
			caName = (*pRxPDO)->m_csName;
			fprintf(pfile, "\t%s\n", caName.c_str());
			fprintf(pfile, "\tPDO Offset: %d\n", (*pRxPDO)->m_iOffset);
			fprintf(pfile, "\tGDE Offset: %d\n", (*pRxPDO)->m_dwGDEOffset);
			fprintf(pfile, "\tByte Offset : %d\n", (*pRxPDO)->m_dwByteOffset);
			fprintf(pfile, "\tPDO Type  : %d\n", (*pRxPDO)->m_dwGDEType);
			fprintf(pfile, "\tData Size : %d\n", (*pRxPDO)->m_iDataSize);
			fprintf(pfile, "\tRun Type : %d\n", (*pRxPDO)->m_dwRunType);
			fprintf(pfile, "\n");
		}
	}
	fclose(pfile);
}
