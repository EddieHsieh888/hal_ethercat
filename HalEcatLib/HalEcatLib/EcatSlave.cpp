#include "stdafx.h"
#include "EcatSlave.h"
#include "RTXEcatSlave.h"
#include "RegAPI.h"
// ******************************************************************
// *****		CEcatPDO Object									*****
// ******************************************************************

IMPLEMENT_SERIAL(CEcatPDO,CObject,0)
void CEcatPDO::Serialize(CArchive& ar)
{
 CObject::Serialize(ar);
 DWORD PDO_version = 101; // 2018-01-31 Add m_dwByteOffset Member Variable

 if (ar.IsStoring())
 {    
	 ar.Write(&PDO_version,		sizeof(DWORD));
	 ar.Write(&m_iOffset,		sizeof(int));
	 ar.Write(&m_iDataSize,		sizeof(int));
	 ar << m_csTagname;
	 ar << m_csName;
	 ar << m_csComment;
	 ar.Write(&m_dwGDEOffset,	sizeof(DWORD));
	 ar.Write(&m_dwGDEType,		sizeof(DWORD));
	 ar.Write(&m_dwByteOffset,	sizeof(DWORD));
	 ar.Write(&m_dwRunType,		sizeof(DWORD));
 }
 else
 {
	ar.Read(&PDO_version,		sizeof(DWORD));
	ar.Read(&m_iOffset,			sizeof(int));
	ar.Read(&m_iDataSize,		sizeof(int));
	ar >> m_csTagname;
	ar >> m_csName;
	ar >> m_csComment;
	ar.Read(&m_dwGDEOffset,		sizeof(DWORD));
	ar.Read(&m_dwGDEType,		sizeof(DWORD));
	if (PDO_version >= 101)
	{
		ar.Read(&m_dwByteOffset,		sizeof(DWORD));
		ar.Read(&m_dwRunType,			sizeof(DWORD));
	}
 }
}

void CEcatPDO::ConvertToRTX(CRTXEcatPDO *pRTXPdo)
{
	char szName[MAX_PATH];
	ZeroMemory(szName, MAX_PATH);
	wcstombs(szName, m_csName, m_csName.GetLength());
	pRTXPdo->m_csName      = szName;
	pRTXPdo->m_iOffset     = m_iOffset;
	pRTXPdo->m_dwGDEOffset = m_dwGDEOffset;
	pRTXPdo->m_dwGDEType   = m_dwGDEType;
	pRTXPdo->m_iDataSize   = m_iDataSize;
	pRTXPdo->m_dwByteOffset   = m_dwByteOffset;
	pRTXPdo->m_dwRunType   = m_dwRunType;
}
// ******************************************************************
// *****		CEcatSlave Object								*****
// ******************************************************************

IMPLEMENT_SERIAL(CEcatSlave,CObject,0)
void CEcatSlave::Serialize(CArchive& ar)
{
 CObject::Serialize(ar);
 DWORD Slave_version = 101;

 if (ar.IsStoring())
 {   
	ar.Write(&Slave_version,		sizeof(DWORD));
	ar.Write(&m_dwSlaveID,			sizeof(DWORD));
	ar.Write(&m_dwRevisionID,		sizeof(DWORD));
	ar.Write(&m_dwEcatType,			sizeof(DWORD));
	ar.Write(&m_dwPhysicalAddress,	sizeof(DWORD));
	ar.Write(&m_dwMotionMode,		sizeof(DWORD));
	ar << m_csName;
	m_TxPDO.Serialize(ar);
	m_RxPDO.Serialize(ar);
 }
 else
 {
	ar.Read(&Slave_version,		sizeof(DWORD));
	ar.Read(&m_dwSlaveID,		sizeof(DWORD));
	ar.Read(&m_dwRevisionID,	sizeof(DWORD));
	ar.Read(&m_dwEcatType,		sizeof(DWORD));
	ar.Read(&m_dwPhysicalAddress,sizeof(DWORD));
	if (Slave_version > 100) // Version 101
		ar.Read(&m_dwMotionMode,sizeof(DWORD));
	ar >> m_csName;
	m_TxPDO.Serialize(ar);
	m_RxPDO.Serialize(ar);
 }
}

void CEcatSlave::ConvertToRTX(CRTXEcatSlave *pRTXSlave)
{
	char szName[MAX_PATH];
	ZeroMemory(szName, MAX_PATH);
	wcstombs(szName, m_csName, m_csName.GetLength());
	pRTXSlave->m_csName				= szName;
	pRTXSlave->m_dwSlaveID			= m_dwSlaveID;
	pRTXSlave->m_iRxPDONum			= m_RxPDO.GetCount();
	pRTXSlave->m_iTxPDONum			= m_TxPDO.GetCount();
	pRTXSlave->m_dwRevisionID		= m_dwRevisionID;
	pRTXSlave->m_dwEcatType			= m_dwEcatType;
	pRTXSlave->m_dwPhysicalAddress	= m_dwPhysicalAddress;
	pRTXSlave->m_dwMotionMode		= m_dwMotionMode;		
}

// ******************************************************************
// *****		CEcatMaster Object								*****
// ******************************************************************

BOOL CEcatMaster::SaveObject(char *filename)
{
	char file_cnf[MAX_PATH] = "PDO";
	if (filename == NULL)
	{
 		if (!RetrieveFilePath("PDO",file_cnf))
 		{
			MessageBox(GetForegroundWindow(), _T("Improper File Initialization (PDO=?)"), _T("ECATPDO"), MB_OK | MB_ICONEXCLAMATION);
			return FALSE;
		} 	    
	}
	else
		strcpy(file_cnf, filename);

	CFile cnfFile;
	BOOL  ret;
	ret = cnfFile.Open((CString)file_cnf,CFile::modeCreate|CFile::modeWrite);
	if(!ret) 
	{
		MessageBox(GetForegroundWindow(), _T("Fail to create PDO configuration file"), _T("ECATPDO"), MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	CArchive ar(&cnfFile, CArchive::store);
	m_ECatSlave.Serialize(ar);
	ar.Close();
	cnfFile.Close();
	return TRUE;
}

BOOL CEcatMaster::LoadSetting(char *filename)
{ 
 	char file_cnf[MAX_PATH] = "PDO";
	if (filename == NULL)
	{
 		if (!RetrieveFilePath("PDO",file_cnf))
 		{
  		MessageBox(GetForegroundWindow(), _T("Improper File Initialization (PDO=?)"), _T("ECATPDO"), MB_OK | MB_ICONEXCLAMATION);
  		return FALSE;
		} 	             	
	}
	else
		strcpy(file_cnf, filename);
	
	CFile cnfFile;
	BOOL  ret;
	ret = cnfFile.Open((CString)file_cnf,CFile::modeRead);
	if(!ret) 
	{
		MessageBox(GetForegroundWindow(), _T("Fail to open PDO configuration file"), _T("ECATPDO"), MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}	
	CArchive ar(&cnfFile, CArchive::load);
	m_ECatSlave.Serialize(ar);
	ar.Close();
	cnfFile.Close();
	
	// test under developing 
#if 0
	CEcatSlave *pSlave;
	POSITION pos = m_ECatSlave.GetHeadPosition();
	while (pos)
	{
		pSlave = (CEcatSlave *) m_ECatSlave.GetAt(pos);
		if (pSlave)
		{
			CEcatPDO* pObj;
			POSITION  cpos = pSlave->m_TxPDO.GetHeadPosition();
			while( cpos != NULL)
			{
				pObj = (CEcatPDO *)pSlave->m_TxPDO.GetAt(cpos);
				pSlave->m_TxPDO.GetNext(cpos);  
			}

			cpos = pSlave->m_RxPDO.GetHeadPosition();
			while( cpos != NULL)
			{
				pObj = (CEcatPDO *)pSlave->m_RxPDO.GetAt(cpos);
				pSlave->m_RxPDO.GetNext(cpos);  
			}
		}
		m_ECatSlave.GetNext(pos);
	}
#endif
	return TRUE;
}

void CEcatMaster::ClearObject()
{
	// Clear Object memory
	POSITION pos = m_ECatSlave.GetHeadPosition();
	while (pos)
	{
		CEcatSlave *pObj = (CEcatSlave *) m_ECatSlave.GetAt(pos);
		if (pObj)
			delete pObj;
		m_ECatSlave.GetNext(pos);
	}
	m_ECatSlave.RemoveAll();
}


void CEcatMaster::ConvertToRTX(CRTXEcatMaster* pRTXEcatMaster)
{
	// test under developing 
	CEcatSlave *pSlave;
	CRTXEcatSlave *pRTXSlave;
	POSITION pos = m_ECatSlave.GetHeadPosition();
	while (pos)
	{
		pSlave = (CEcatSlave *) m_ECatSlave.GetAt(pos);
		if (pSlave)
		{
			pRTXSlave = new CRTXEcatSlave;
			pSlave->ConvertToRTX(pRTXSlave);
			pRTXEcatMaster->m_ECatSlave.push_back(pRTXSlave);

			CEcatPDO* pObj;
			CRTXEcatPDO* pRTXObj;
			POSITION  cpos = pSlave->m_TxPDO.GetHeadPosition();
			while( cpos != NULL)
			{
				pObj = (CEcatPDO *)pSlave->m_TxPDO.GetAt(cpos);
				if (pObj)
				{
					pRTXObj = new CRTXEcatPDO;
					pObj->ConvertToRTX(pRTXObj);
					pRTXSlave->m_TxPDO.push_back(pRTXObj);
				}
				pSlave->m_TxPDO.GetNext(cpos);  
			}

			cpos = pSlave->m_RxPDO.GetHeadPosition();
			while( cpos != NULL)
			{
				pObj = (CEcatPDO *)pSlave->m_RxPDO.GetAt(cpos);
				if (pObj)
				{
					pRTXObj = new CRTXEcatPDO;
					pObj->ConvertToRTX(pRTXObj);
					pRTXSlave->m_RxPDO.push_back(pRTXObj);
				}
				pSlave->m_RxPDO.GetNext(cpos);  
			}
		}
		m_ECatSlave.GetNext(pos);
	}
}