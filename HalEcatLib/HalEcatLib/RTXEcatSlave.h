#ifndef _RTXECAT_SLAVE_H_
#define _RTXECAT_SLAVE_H_
#include <string>
#include <list>
using namespace std;

#define MAX_RTXPDO_INDEX 32

#define MOTION_MODE_CSP		8
#define MOTION_MODE_CSV		9
#define MOTION_MODE_CST		10
class CRTXEcatPDO
{
public:
	CRTXEcatPDO()
	{
		m_csName.empty();
		m_csComment.empty();
		m_csTagname.empty();
		m_iDataSize = 0;
		m_iOffset   = 0;
		m_dwGDEOffset = 0;
		m_dwGDEType = 0;
		m_dwByteOffset = 0;
		m_dwRunType = 0;
		m_pPDOOutputPointer = NULL;
		m_pPDOInputPointer  = NULL;
	};
	~CRTXEcatPDO()
	{
	};

	bool SetSlaveValue(DWORD value);
	bool GetSlaveValue(DWORD *pvalue);


public:
	string m_csName;
	string m_csComment;
	int		m_iOffset;
	int     m_iDataSize;
	//WINPC32 
	string	m_csTagname;
	DWORD	m_dwGDEOffset;
	DWORD	m_dwGDEType;
	DWORD	m_dwByteOffset;
	DWORD	m_dwRunType;

	BYTE*	m_pPDOOutputPointer;
	BYTE*	m_pPDOInputPointer;
	BYTE	m_bitMask;
};

typedef list<CRTXEcatPDO *> PDOLIST;

class CRTXEcatSlave
{
public:
	CRTXEcatSlave()
	{
		m_csName.empty();
		m_dwSlaveID				= 0;
		m_dwRevisionID			= 0;
		m_dwEcatType			= 0;
		m_dwPhysicalAddress		= 0;
		m_iInputPDOStartAddress = 0;
		m_iOutpuPDOtStartAddress= 0;
		m_iTxPDONum             = 0;
		m_iRxPDONum             = 0;
		m_dwPhysicalAddrss		= 0;
		m_dwMotionMode			= 0;
		m_TxPDO.clear();
		m_RxPDO.clear();
		for (int i = 0; i < MAX_RTXPDO_INDEX; i++)
		{
			m_iInputPDOIndex[i]  = 0;
			m_iOutputPDOIndex[i] = 0;
		}
		m_pPDORxPointer = NULL;
		m_pPDOTxPointer = NULL;
	};

	~CRTXEcatSlave()
	{
		// Clear TX PDO
		PDOLIST::iterator i;
		for (i = m_TxPDO.begin(); i != m_TxPDO.end(); ++i)
		{
			delete (*i);
		}
		m_TxPDO.clear();

		// Clear RX PDO
		for (i = m_RxPDO.begin(); i != m_RxPDO.end(); ++i)
		{
			delete (*i);
		}
		m_RxPDO.clear();

	};

public:
	void	ECatInput(DWORD dwType);
	void	ECatOutput(DWORD dwType);
	void	ECatSetInputPointer(BYTE* pTxPointer);
	void	ECatSetOutputPointer(BYTE* pRxPointer);

	string  m_csName;
	DWORD   m_dwSlaveID;
	DWORD   m_dwRevisionID;
	DWORD   m_dwEcatType;
	DWORD	m_dwPhysicalAddress;
	DWORD	m_dwMotionMode;

	DWORD	m_dwPhysicalAddrss;
	int     m_iInputPDOStartAddress;
	int     m_iOutpuPDOtStartAddress;

	int		m_iInputPDOIndex[MAX_RTXPDO_INDEX];
	int		m_iOutputPDOIndex[MAX_RTXPDO_INDEX];
	
	int		m_iTxPDONum;
	int		m_iRxPDONum;

	PDOLIST m_TxPDO;
	PDOLIST m_RxPDO;
	BYTE	*m_pPDORxPointer;
	BYTE	*m_pPDOTxPointer;

};


// ********************************************************************************
// Axis Class
// ********************************************************************************
typedef enum _T_Motion_Command
{
    command_none			= 0,
	command_stop			= 1,
	command_start			= 2,
	command_home			= 3,
	command_run				= 4
} T_Motion_Command;

typedef enum _T_DS402_State
{
	state_not_ready          = 0,
	state_switchon_dis       = 1,
	state_ready_to_switchon  = 2,
	state_switched_on        = 3,
	state_op_enabled         = 4,
	state_quick_stop         = 5,
	state_malfunction        = 6,
	state_not_home			 = 7,
	state_homing			 = 8
} T_DS402_State;


// EtherCAT Slave Type  Check 
#define SERVO_DEVICE		402
#define IO_DEVICE			401

#define YASKAWA_SERVO		 0x02200001
#define YASKAWA_SERVO_SIGMA7 0x02200301
#define HAL_RMB				 0x54490001
class RTXSlaveAxis
{
public:
	RTXSlaveAxis()
	{
		m_wControl	= 0;
		m_cMode		= 9;
		m_wChannel	= 0;
		m_dwProductID  = 0;
		m_dwRevisionID = 0;
		m_dwPhysicalAddrss = 0;

		m_pwStatusWord  = NULL;
		m_pwControlWord = NULL;
		m_pwErrorCode	= NULL;
		m_pdwActualPosition = NULL;
		m_pdwTargetVelocity	= NULL;
	};

	RTXSlaveAxis(DWORD slaveID, DWORD revID, DWORD addr)
	{
		m_wControl	= 0;
		m_cMode		= 9;
		m_wChannel	= 0;
		m_dwProductID  = slaveID;
		m_dwRevisionID = revID;
		m_dwPhysicalAddrss = addr;

		m_pwStatusWord  = NULL;
		m_pwControlWord = NULL;
		m_pwErrorCode	= NULL;
		m_pdwActualPosition = NULL;
		m_pdwTargetVelocity	= NULL;

	};
	void StateMachine();

public:
	T_DS402_State		m_wState;
	T_Motion_Command	m_dwCommand;
	unsigned short		m_wControl;
    unsigned char		m_cMode;

	DWORD				m_wChannel;
	DWORD				m_dwProductID;
	DWORD				m_dwRevisionID;
	DWORD				m_dwPhysicalAddrss;

	WORD*  m_pwStatusWord;
	WORD*  m_pwControlWord;
	WORD*  m_pwErrorCode;
	DWORD* m_pdwActualPosition;
	DWORD* m_pdwTargetVelocity;
};

typedef list<RTXSlaveAxis *> AXISLIST;
typedef list<CRTXEcatSlave *> SLAVELIST;
class CRTXEcatMaster
{
public: 
	CRTXEcatMaster()
	{
		m_ECatSlave.clear();
		m_ECatAxis.clear();
	};

	~CRTXEcatMaster()
	{
		m_ECatSlave.clear();
		m_ECatAxis.clear();
	};

public:

	SLAVELIST m_ECatSlave;
	AXISLIST  m_ECatAxis;
	void	  ClearObject();
	SLAVELIST*  GetList(){ return &m_ECatSlave;}
	void	  PrintOut();
	void		GenerateAxisList();
	void		ClearAxisList();
	AXISLIST*	GetAxisList(){ return &m_ECatAxis;}
	void		PrintOutAxisList();

};
#endif //