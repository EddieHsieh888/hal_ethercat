#include "stdafx.h"
#include "RTXEcatSlave.h"
#include <iostream>
#include "memapi.h"
#include "vramdef.h"

#ifdef _CRITICAL_SECTION_
	extern CRITICAL_SECTION g_criticalSection; 
#endif

BYTE BIT_MASK[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
bool CRTXEcatPDO::SetSlaveValue(DWORD value)
{
	if (!m_pPDOOutputPointer) return FALSE;
	switch (m_iDataSize)
	{
		case 1:		// Bit
			if (value)
				*(BYTE *)m_pPDOOutputPointer |= BIT_MASK[m_bitMask];
			else		
				*(BYTE *)m_pPDOOutputPointer &= (~BIT_MASK[m_bitMask]);
			break;
		case 8:		// BYTE
			*(BYTE *)m_pPDOOutputPointer = (BYTE)value;
			break;
		case 16:	// WORD
			*(WORD *)m_pPDOOutputPointer = (WORD)value;
			break;
		case 32:	// DWORD
			*(DWORD *)m_pPDOOutputPointer = (DWORD)value;
			break;
		case 64:	// DOUBLE
			*(DWORD *)m_pPDOOutputPointer = (DWORD)value;
			break;
		default:
			return false;

	}
	return true;
}

bool CRTXEcatPDO::GetSlaveValue(DWORD *pValue)
{
	if (!m_pPDOInputPointer) return FALSE;
	switch (m_iDataSize)
	{
		case 1:		// Bit
			if (*(BYTE *)m_pPDOInputPointer & BIT_MASK[m_bitMask])
				*pValue = TRUE;
			else 
				*pValue = FALSE;
			break;
		case 8:		// BYTE
			*pValue = *(BYTE *)m_pPDOInputPointer;
			break;
		case 16:	// WORD
			*pValue = *(WORD *)m_pPDOInputPointer;
			break;
		case 32:	// DWORD
			*pValue = *(DWORD *)m_pPDOInputPointer;
			break;
		case 64:	// DOUBLE
			*pValue = *(DWORD *)m_pPDOInputPointer;
			break;
		default:
			return false;
			break;
	}
	return true;
}

void CRTXEcatSlave::ECatSetInputPointer(BYTE* pTxPointer)
{
	m_pPDOTxPointer = pTxPointer;
	PDOLIST::iterator pTxPDO;
	//"=======[Slave TX PDO] ============"
	for (pTxPDO = m_TxPDO.begin(); pTxPDO != m_TxPDO.end(); ++pTxPDO)
	{
		(*pTxPDO)->m_pPDOInputPointer = pTxPointer + ((*pTxPDO)->m_iOffset)/8;
		(*pTxPDO)->m_bitMask		  = ((*pTxPDO)->m_iOffset) % 8;
		if ((*pTxPDO)->m_dwGDEType != 0)
			RtPrintf("PDO Input [%d][%d][%d] = 0x%X\n", (*pTxPDO)->m_dwGDEType, (*pTxPDO)->m_dwGDEOffset, (*pTxPDO)->m_iOffset, (*pTxPDO)->m_pPDOInputPointer);
	}
}

DWORD predwR0578 = 0;
DWORD predwR0579 = 0;
int i578Cnt = 0;
int i579Cnt = 0;
void CRTXEcatSlave::ECatInput(DWORD dwType)
{
	DWORD value = 0;
	DWORD dwOriValue = 0;
	DWORD dwMask8Bit[4] = {0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000};
	DWORD dwShift8Bit[4] = {0, 8, 16, 24};
	DWORD dwMask16Bit[2] = {0x0000FFFF, 0xFFFF0000};
	DWORD dwShift16Bit[2] = {0, 16};

	PDOLIST::iterator pTxPDO;
	//"=======[Slave TX PDO] ============"
	for (pTxPDO = m_TxPDO.begin(); pTxPDO != m_TxPDO.end(); ++pTxPDO)
	{
		if ((*pTxPDO)->m_dwGDEType != 0)
		{
			//if (dwType <= (*pTxPDO)->m_dwRunType)
			{
	#ifdef _CRITICAL_SECTION_
			EnterCriticalSection(&g_criticalSection);
	#endif
				(*pTxPDO)->GetSlaveValue(&value);
	#ifdef _CRITICAL_SECTION_
			LeaveCriticalSection(&g_criticalSection);
	#endif
				switch((*pTxPDO)->m_iDataSize)
				{
					case 8:
					{
						value = value & 0x000000FF;
						value = value << dwShift8Bit[(*pTxPDO)->m_dwByteOffset];
						GDEGetBit((*pTxPDO)->m_dwGDEType, (*pTxPDO)->m_dwGDEOffset, &dwOriValue);
						dwOriValue = dwOriValue & (~dwMask8Bit[(*pTxPDO)->m_dwByteOffset]);
						value = value | dwOriValue;
						break;
					}
					case 16:
					{
						value = value & 0x0000FFFF;
						value = value << dwShift16Bit[(*pTxPDO)->m_dwByteOffset];
						GDEGetBit((*pTxPDO)->m_dwGDEType, (*pTxPDO)->m_dwGDEOffset, &dwOriValue);
						dwOriValue = dwOriValue & (~dwMask16Bit[(*pTxPDO)->m_dwByteOffset]);
						value = value | dwOriValue;
						break;
					}
					case 1:
					case 32:
					case 64:
						break;
				}
				GDESetBit((*pTxPDO)->m_dwGDEType, (*pTxPDO)->m_dwGDEOffset, value);
				//RtPrintf("GDE Type and Address = 0x%X, %d\n", (*pTxPDO)->m_dwGDEType, (*pTxPDO)->m_dwGDEOffset);
	#if 0
				if ((*pTxPDO)->m_dwGDEOffset == 578)
				{
					if (predwR0578 == value)
						GDESetBit(GDE_REGST, 600, i578Cnt++);
					predwR0578 = value;
				}

				if ((*pTxPDO)->m_dwGDEOffset == 579)
				{
					if (predwR0579 == value)
						GDESetBit(GDE_REGST, 601, i579Cnt++);
					predwR0579 = value;
				}
	#endif
			}
		}
	}
}

void CRTXEcatSlave::ECatSetOutputPointer(BYTE* pRxPointer)
{
	m_pPDORxPointer = pRxPointer;
	PDOLIST::iterator pRxPDO;
	// "=======[Slave RX PDO] ============"
	for (pRxPDO = m_RxPDO.begin(); pRxPDO != m_RxPDO.end(); ++pRxPDO)
	{
		(*pRxPDO)->m_pPDOOutputPointer = pRxPointer + ((*pRxPDO)->m_iOffset)/8;;
		(*pRxPDO)->m_bitMask		  = ((*pRxPDO)->m_iOffset) % 8;
		if ((*pRxPDO)->m_dwGDEType != 0)
			RtPrintf("PDO Output [%d][%d][%d] = 0x%X\n", (*pRxPDO)->m_dwGDEType, (*pRxPDO)->m_dwGDEOffset, (*pRxPDO)->m_iOffset, (*pRxPDO)->m_pPDOOutputPointer);

	}
}

void CRTXEcatSlave::ECatOutput(DWORD dwType)
{
	DWORD value = 0;
	DWORD dwShift8Bit[4] = {0, 8, 16, 24};
	DWORD dwShift16Bit[2] = {0, 16};

	PDOLIST::iterator pRxPDO;
	// "=======[Slave RX PDO] ============"
	for (pRxPDO = m_RxPDO.begin(); pRxPDO != m_RxPDO.end(); ++pRxPDO)
	{
		if ((*pRxPDO)->m_dwGDEType != 0)
		{
			GDEGetBit((*pRxPDO)->m_dwGDEType, (*pRxPDO)->m_dwGDEOffset, &value);
			switch((*pRxPDO)->m_iDataSize)
			{
				case 8:
				{
					value = value >> dwShift8Bit[(*pRxPDO)->m_dwByteOffset];
					value = value & 0x000000FF;
					break;
				}
				case 16:
				{
					value = value >> dwShift16Bit[(*pRxPDO)->m_dwByteOffset];
					value = value & 0x0000FFFF;
					break;
				}
				case 1:
				case 32:
				case 64:
					break;
			}
#ifdef _CRITICAL_SECTION_
		EnterCriticalSection(&g_criticalSection);
#endif
		if (dwType == 255)
			(*pRxPDO)->SetSlaveValue(0);
		else
		{
			//if (dwType <= (*pRxPDO)->m_dwRunType)
				(*pRxPDO)->SetSlaveValue(value);
		}
#ifdef _CRITICAL_SECTION_
		LeaveCriticalSection(&g_criticalSection);
#endif
		}
	}
}
void CRTXEcatMaster::ClearObject()
{
	string caName;

	SLAVELIST::iterator pSlavelist;
	for (pSlavelist = m_ECatSlave.begin(); pSlavelist != m_ECatSlave.end(); ++pSlavelist)
	{

		caName = (*pSlavelist)->m_csName;
		/*
		PDOLIST::iterator pTxPDO;
		for (pTxPDO = ((*pSlavelist)->m_TxPDO).begin(); pTxPDO != ((*pSlavelist)->m_TxPDO).end(); ++pTxPDO)
		{
			caName = (*pTxPDO)->m_csName;
			delete (*pTxPDO);
		}

		PDOLIST::iterator pRxPDO;
		for (pRxPDO = (*pSlavelist)->m_RxPDO.begin(); pRxPDO !=(*pSlavelist)->m_RxPDO.end(); ++pRxPDO)
		{
			caName = (*pRxPDO)->m_csName;
			delete (*pRxPDO);
		}
		*/
		delete (*pSlavelist);
	}
}

void CRTXEcatMaster::PrintOut()
{
	FILE *pfile = fopen("\\HDD\\PrintOutRTXFormat.txt", "w");
	string caName;
	SLAVELIST::iterator pSlavelist;
	for (pSlavelist = m_ECatSlave.begin(); pSlavelist != m_ECatSlave.end(); ++pSlavelist)
	{

		caName = (*pSlavelist)->m_csName;
		fprintf(pfile, "[%s]\n",				caName.c_str());
		fprintf(pfile, "Slave ID = 0x%X\n",		(*pSlavelist)->m_dwSlaveID);
		fprintf(pfile, "RevisionID = 0x%X\n",	(*pSlavelist)->m_dwRevisionID);
		fprintf(pfile, "ECAT Type = 0x%X\n",	(*pSlavelist)->m_dwEcatType);
		fprintf(pfile, "Physical Address = %d\n",	(*pSlavelist)->m_dwPhysicalAddress);

		PDOLIST::iterator pTxPDO;
		fprintf(pfile, "=======[TX PDO] ============\n");
		for (pTxPDO = ((*pSlavelist)->m_TxPDO).begin(); pTxPDO != ((*pSlavelist)->m_TxPDO).end(); ++pTxPDO)
		{
			caName = (*pTxPDO)->m_csName;
			fprintf(pfile, "\t%s\n", caName.c_str());
			fprintf(pfile, "\tPDO Offset: %d\n", (*pTxPDO)->m_iOffset);
			fprintf(pfile, "\tGDE Offset: %d\n", (*pTxPDO)->m_dwGDEOffset);
			fprintf(pfile, "\tByte Offset : %d\n", (*pTxPDO)->m_dwByteOffset);
			fprintf(pfile, "\tPDO Type  : %d\n", (*pTxPDO)->m_dwGDEType);
			fprintf(pfile, "\tData Size : %d\n", (*pTxPDO)->m_iDataSize);
			fprintf(pfile, "\tRun Type : %d\n", (*pTxPDO)->m_dwRunType);
			fprintf(pfile, "\n");
		}

		PDOLIST::iterator pRxPDO;
		fprintf(pfile, "=======[RX PDO] ============\n");
		for (pRxPDO = (*pSlavelist)->m_RxPDO.begin(); pRxPDO !=(*pSlavelist)->m_RxPDO.end(); ++pRxPDO)
		{
			caName = (*pRxPDO)->m_csName;
			fprintf(pfile, "\t%s\n", caName.c_str());
			fprintf(pfile, "\tPDO Offset: %d\n", (*pRxPDO)->m_iOffset);
			fprintf(pfile, "\tGDE Offset: %d\n", (*pRxPDO)->m_dwGDEOffset);
			fprintf(pfile, "\tByte Offset : %d\n", (*pRxPDO)->m_dwByteOffset);
			fprintf(pfile, "\tPDO Type  : %d\n", (*pRxPDO)->m_dwGDEType);
			fprintf(pfile, "\tData Size : %d\n", (*pRxPDO)->m_iDataSize);
			fprintf(pfile, "\tRun Type : %d\n", (*pRxPDO)->m_dwRunType);
			fprintf(pfile, "\n");
		}
	}
	fclose(pfile);
}


void CRTXEcatMaster::GenerateAxisList()
{
	RTXSlaveAxis *pObj = NULL;
	string caName;
	SLAVELIST::iterator pSlavelist;
	PDOLIST::iterator pPDOlist;

	for (pSlavelist = m_ECatSlave.begin(); pSlavelist != m_ECatSlave.end(); ++pSlavelist)
	{
		caName = (*pSlavelist)->m_csName;
		if ((*pSlavelist)->m_dwEcatType == SERVO_DEVICE)
		{
			// SERVO_DEIVCE CoE-402
			switch((*pSlavelist)->m_dwSlaveID)
			{
				case YASKAWA_SERVO:
				case YASKAWA_SERVO_SIGMA7:
					{
						pObj = new RTXSlaveAxis((*pSlavelist)->m_dwSlaveID, (*pSlavelist)->m_dwRevisionID, (*pSlavelist)->m_dwPhysicalAddrss);
						if (pObj)
						{

							// asign the control word/status word pointer
							for (pPDOlist = ((*pSlavelist)->m_RxPDO).begin();  pPDOlist != ((*pSlavelist)->m_RxPDO).end(); ++pPDOlist)
							{
								//RtPrintf("RxPDO Name = %s, Output Pointer = 0x%X, Input Pointer = 0x%X\n", (*pPDOlist)->m_csName.c_str(), (*pPDOlist)->m_pPDOOutputPointer, (*pPDOlist)->m_pPDOInputPointer);
								if ((*pPDOlist)->m_csName.find("Control word") != -1)
								{
									pObj->m_pwControlWord = (WORD *)((*pPDOlist)->m_pPDOOutputPointer);
									//RtPrintf("Found Control word pointer =0x%X\n", pObj->m_pwControlWord);
								}

								if ((*pSlavelist)->m_dwMotionMode == MOTION_MODE_CSV || (*pSlavelist)->m_dwMotionMode == 0)
								{
									if ((*pPDOlist)->m_csName.find("Target velocity") != -1)
									{
										pObj->m_pdwTargetVelocity  = (DWORD *)((*pPDOlist)->m_pPDOOutputPointer);
										//RtPrintf("Found Target velocity pointer 0x%X\n", pObj->m_pdwTargetVelocity);
									}
									//RtPrintf("CSV Mode ....\n");
								}
								else if ((*pSlavelist)->m_dwMotionMode == MOTION_MODE_CSP)
								{
									if ((*pPDOlist)->m_csName.find("Target position") != -1)
										pObj->m_pdwTargetVelocity  = (DWORD *)((*pPDOlist)->m_pPDOOutputPointer);

									//RtPrintf("CSP Mode ....\n");
								}
								else
									RtPrintf("Not Support Motion Mode = %d ....\n", (*pSlavelist)->m_dwMotionMode);

							}

							for (pPDOlist = ((*pSlavelist)->m_TxPDO).begin();  pPDOlist != ((*pSlavelist)->m_TxPDO).end(); ++pPDOlist)
							{
								//RtPrintf("TxPDO Name = %s, Output Pointer = 0x%X, Input Pointer = 0x%X\n", (*pPDOlist)->m_csName.c_str(), (*pPDOlist)->m_pPDOOutputPointer, (*pPDOlist)->m_pPDOInputPointer);
								if ((*pPDOlist)->m_csName.find("Status word") != -1)
								{
									pObj->m_pwStatusWord = (WORD *)((*pPDOlist)->m_pPDOInputPointer);
									//RtPrintf("Found Status word pointer = 0x%X\n", pObj->m_pwStatusWord);
								}

								if ((*pPDOlist)->m_csName.find("Position actual value") != -1)
								{
									pObj->m_pdwActualPosition = (DWORD *)((*pPDOlist)->m_pPDOInputPointer);
									//RtPrintf("Found Position value pointer = 0x%X\n", pObj->m_pdwActualPosition);
								}

								if ((*pPDOlist)->m_csName.find("Error code") != -1)
								{
									pObj->m_pwErrorCode = (WORD *)((*pPDOlist)->m_pPDOInputPointer);
								}
							}
							m_ECatAxis.push_back(pObj);
						}
						break;
					}
				case HAL_RMB:
					{
						for (int i= 0; i < 4; i++)
						{
							pObj = new RTXSlaveAxis((*pSlavelist)->m_dwSlaveID, (*pSlavelist)->m_dwRevisionID, (*pSlavelist)->m_dwPhysicalAddrss);
							if (pObj)
							{
								// asign the control word/status word pointer
								pObj->m_wChannel = i;
								m_ECatAxis.push_back(pObj);
							}

							int  iControlWordFoundNo = 0;
							int  iStatusWordFoundNo  = 0;
							int  iTargetVelFoundNo   = 0;
							int  iActualPosFoundNo   = 0;
							int  iErrorCodeFoundNo   = 0;

							if (pObj)
							{
								// asign the control word/status word pointer
								for (pPDOlist = ((*pSlavelist)->m_RxPDO).begin();  pPDOlist != ((*pSlavelist)->m_RxPDO).end(); ++pPDOlist)
								{
									string str = (*pPDOlist)->m_csName;
									if ((*pPDOlist)->m_csName.find("Control Word") != -1)
									{
										if (iControlWordFoundNo == i)
											pObj->m_pwControlWord = (WORD *)((*pPDOlist)->m_pPDOOutputPointer);

										iControlWordFoundNo++;
									}

									if ((*pPDOlist)->m_csName.find("TargetVelocity") != -1)
									{
										if (iTargetVelFoundNo == i)
											pObj->m_pdwTargetVelocity  = (DWORD *)((*pPDOlist)->m_pPDOOutputPointer);
										iTargetVelFoundNo++;
									}
								}

								for (pPDOlist = ((*pSlavelist)->m_TxPDO).begin();  pPDOlist != ((*pSlavelist)->m_TxPDO).end(); ++pPDOlist)
								{
									string str = (*pPDOlist)->m_csName;
									if ((*pPDOlist)->m_csName.find("Status Word") != -1)
									{
										if (iStatusWordFoundNo == i)
											pObj->m_pwStatusWord = (WORD *)((*pPDOlist)->m_pPDOInputPointer);
										iStatusWordFoundNo++;
									}

									if ((*pPDOlist)->m_csName.find("ActualPosition") != -1)
									{
										if (iActualPosFoundNo == i)
											pObj->m_pdwActualPosition = (DWORD *)((*pPDOlist)->m_pPDOInputPointer);
										iActualPosFoundNo++;
									}

									if ((*pPDOlist)->m_csName.find("Error Code") != -1)
									{
										if (iErrorCodeFoundNo == i)
											pObj->m_pwErrorCode = (WORD *)((*pPDOlist)->m_pPDOInputPointer);
										iErrorCodeFoundNo++;
									}

								}
							}
						}
						break;
					}
			}
		}
		else
		{
			// IO_DEIVCE CoE-0/CoE-401

		}
	}
}

void CRTXEcatMaster::ClearAxisList()
{
	AXISLIST::iterator pAxislist;
	for (pAxislist = m_ECatAxis.begin(); pAxislist != m_ECatAxis.end(); ++pAxislist)
	{
		if ((*pAxislist))
			delete (*pAxislist);
	}
}

void CRTXEcatMaster::PrintOutAxisList()
{
	AXISLIST::iterator pAxislist;
	for (pAxislist = m_ECatAxis.begin(); pAxislist != m_ECatAxis.end(); ++pAxislist)
	{
		if ((*pAxislist))
		{
			RtPrintf("==========================================\n");
			RtPrintf("ProduceID = 0x%X\n", (*pAxislist)->m_dwProductID);
			RtPrintf("Control Word	= 0x%X\n", (*pAxislist)->m_pwControlWord);
			RtPrintf("Status Word	= 0x%X\n", (*pAxislist)->m_pwStatusWord);
			RtPrintf("Error Code Word	= 0x%X\n", (*pAxislist)->m_pwErrorCode);
		}
	}
}

// *******************************************************************************
// AXIS Object
// *******************************************************************************

int	ControlMode			   = 1;
WORD g_wOldRMBStatus       = 0;
T_Motion_Command g_dwOldRMBCommand = command_home;

void  RTXSlaveAxis::StateMachine()
{
	if (m_pwStatusWord == NULL || m_pdwActualPosition == NULL || m_pwControlWord == NULL || m_pdwTargetVelocity == NULL)
	{
		//dprintf("PDO Input or Output pointer is NULL.\n");
		return;
	}
	unsigned short wStatus = *m_pwStatusWord; // Status Word PDO Input;

	/*
	if (g_wOldRMBStatus!= wStatus)
		RtPrintf("[CiA402] Status Change: [0x%X] -> [0x%X] @ state=[0x%X]\n", g_wOldRMBStatus, wStatus, TISlave.wState);
	if (g_dwOldRMBCommand != TISlave.dwCommand)
		RtPrintf("[CiA402] Command Change:[0x%X] -> [0x%X] @ state=[0x%X]\n", g_dwOldRMBCommand, TISlave.dwCommand, TISlave.wState);
	g_wOldRMBStatus = wStatus;
	g_dwOldRMBCommand = TISlave.dwCommand;
	*/
	if (m_dwCommand != command_stop)
	{
		// Action
		switch (m_wState) 
		{
			case state_not_ready:
				break;
			case state_switchon_dis:    
				m_wControl = 0x06; //shutdown						--> Enter state_ready_to_switchon
				dprintf("[%d] Switch Disable\n", m_wChannel);
				break;
			case state_ready_to_switchon:   
				m_wControl = 0x07; //Disable Operation/Switch On	--> Enter (state_switched_on)
				dprintf("[%d] Control Switch On\n", m_wChannel);
				break;
			case state_switched_on:
				m_wControl = 0x0F; // Enable Operation				--> Enter (Operation Enable)
				dprintf("[%d] Switch Operation Mode\n", m_wChannel);
				break;
			case state_malfunction:
				if (m_pwErrorCode)
				{
					dprintf("[STATE_FAULT] Error Code [%d]= 0x%X\n", m_wChannel,*m_pwErrorCode);
					if (*m_pwErrorCode !=0)						
						m_wControl = 0x80;//Fault Reset					--> Fault Reset (back to Switch on disable)
				}
				else
					m_wControl = 0x80;//Fault Reset					--> Fault Reset (back to Switch on disable)
				break;
			case state_op_enabled:
				//*m_pdwTargetVelocity = 100000; //Yaskawa
				//*pdwTargetVelocity = 1000; //RMB
				break;
		}

		// State Status
		if (wStatus & 0x08) //Fault
		{
			m_wState = state_malfunction;
			dprintf("[%d] Fault State\n", m_wChannel);
		}
		else if ((wStatus & 0x6F) == 0x21)
		{
			m_wState = state_ready_to_switchon;
			dprintf("[%d] Ready to switch on\n", m_wChannel);
		}
		else if ((wStatus & 0x6F) == 0x23)
		{
			m_wState = state_switched_on;
			dprintf("[%d] Switch on\n", m_wChannel);
		}
		else if ((wStatus & 0x6F) == 0x27)
		{
			m_wState = state_op_enabled;
		}
		else if ((wStatus & 0x4F) == 0x00)
		{
			m_wState = state_not_ready;
			dprintf("[%d] =Not Ready=\n", m_wChannel);
		}
		else if ((wStatus & 0x4F) == 0x40) // Swtch on disble (Bit-5 doesn't care)
		{
			m_wState = state_switchon_dis;
			dprintf("[%d] Switch on disable\n", m_wChannel);
		}

	}
	else
	{
		m_wState = state_ready_to_switchon;
		*m_pdwTargetVelocity = 0;
		m_wControl = 0x00;
	}

	if (*m_pwControlWord != m_wControl)
		dprintf("[CiA402] Control Change = [0x%X] -> [0x%X] @ state=[0x%X]\n", *m_pwControlWord, m_wControl, wStatus);

	*m_pwControlWord = m_wControl;	// ControlWord PDO Output
}
