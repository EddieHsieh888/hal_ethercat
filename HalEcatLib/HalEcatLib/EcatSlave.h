#ifndef _ECAT_SLAVE_H_
#define _ECAT_SLAVE_H_

#define MAX_PDO_INDEX 32
class CRTXEcatPDO;
class CRTXEcatSlave;
class CRTXEcatMaster;

class CEcatPDO : public CObject
{
public:
	DECLARE_SERIAL(CEcatPDO)
	CEcatPDO()
	{
		m_csName.Empty();
		m_csComment.Empty();
		m_csTagname.Empty();
		m_iDataSize = 0;
		m_iOffset   = 0;
		m_dwGDEOffset = 0;
		m_dwGDEType = 0;
		m_dwByteOffset = 0;
		m_dwRunType = 0;
	};
	~CEcatPDO()
	{
	};
	virtual void Serialize(CArchive& ar);
	void ConvertToRTX(CRTXEcatPDO *pRTXPDO);

public:
	CString m_csName;
	CString m_csComment;
	int		m_iOffset;
	int     m_iDataSize;
	//WINPC32 
	CString	m_csTagname;
	DWORD	m_dwGDEOffset;
	DWORD	m_dwGDEType;
	DWORD   m_dwByteOffset;
	DWORD	m_dwRunType;
};

class CEcatSlave : public CObject
{
public:
	DECLARE_SERIAL(CEcatSlave)
	CEcatSlave()
	{
		m_csName.Empty();
		m_dwSlaveID				= 0;
		m_dwRevisionID			= 0;
		m_dwEcatType			= 0;
		m_dwPhysicalAddress		= 0;
		m_dwMotionMode			= 0;

		m_iInputPDOStartAddress = 0;
		m_iOutpuPDOtStartAddress= 0;
		m_TxPDO.RemoveAll();
		m_RxPDO.RemoveAll();
		for (int i = 0; i < MAX_PDO_INDEX; i++)
		{
			m_iInputPDOIndex[i]  = 0;
			m_iOutputPDOIndex[i] = 0;
		}
	};

	~CEcatSlave()
	{
		POSITION pos = m_TxPDO.GetHeadPosition();
		while (pos)
		{
			CEcatPDO *pObj = (CEcatPDO *)m_TxPDO.GetAt(pos);
			m_TxPDO.GetNext(pos);
			if (pObj) 
				delete pObj;
		}
		m_TxPDO.RemoveAll();

		pos = m_RxPDO.GetHeadPosition();
		while (pos)
		{
			CEcatPDO *pObj = (CEcatPDO *)m_RxPDO.GetAt(pos);
			m_RxPDO.GetNext(pos);
			if (pObj) 
				delete pObj;
		}
		m_RxPDO.RemoveAll();
	};
	void Serialize(CArchive& ar);
	void ConvertToRTX(CRTXEcatSlave *pRTXSlave);
public:
	CString m_csName;
	DWORD   m_dwSlaveID;
	DWORD	m_dwRevisionID;
	DWORD	m_dwEcatType;
	DWORD	m_dwPhysicalAddress;
	DWORD	m_dwMotionMode;

	int     m_iInputPDOStartAddress;
	int     m_iOutpuPDOtStartAddress;

	int		m_iInputPDOIndex[MAX_PDO_INDEX];
	int		m_iOutputPDOIndex[MAX_PDO_INDEX];
	
	CObList m_TxPDO;
	CObList m_RxPDO;
};

class CEcatMaster
{
public: 
	CEcatMaster()
	{
		m_ECatSlave.RemoveAll();
	};

	~CEcatMaster()
	{
		m_ECatSlave.RemoveAll();
	};

public:
	CObList	  m_ECatSlave;
	BOOL	  SaveObject(char *filename  = NULL);
	BOOL	  LoadSetting(char *filename = NULL);
	void	  ClearObject();
	CObList*  GetList(){ return &m_ECatSlave;}
	void	  ConvertToRTX(CRTXEcatMaster *pRTXMaster);
};
#endif
